# pFoF
## parallel Friends-of-Friends halo finder

pFoF is a Fortran software package based on a distributed implementation of the Friends-of-Friends halo finder algorithm.  
It can be used to analyze output from the RAMSES N-body code for cosmological simulations. It has also been succesfully used to analyze output from the Gadget code. 
This parallel Friends-of-Friends implementation is based on a serial implementation written by Edouard Audit (Maison de la Simulation).  
It has been written by Fabrice Roy, Vincent Bouillot and Yann Rasera (LUTH, Observatoire de Paris, PSL Research University, CNRS, Université Paris Diderot, Sorbonne Paris Cité).  
contact: fabrice.roy@obspm.fr
The GADGET reading function has been written by Benjamin L’Huillier, Korea Astronomy and Space Science Institute, Daejeon, Korea.  

The code also come with a series of tools designed to manage RAMSES lightcone data and to analyze pFoF outputs.  
The tools have been written by Fabrice Roy.  

There are 2 different versions of pFoF: pfof_snap for RAMSES snapshots, pfof_cone for RAMSES lightcones. 

The outputs of pFoF and of the tools use the HDF5 format. The HDF5 output files are self-describing. You can explore them with [HDFView](https://support.hdfgroup.org/products/java/hdfview/) 
and you can easily read them with standard programming languages (C/C++, fortran, python, etc.).  

Please see the wiki for more information: https://gitlab.obspm.fr/roy/pFoF/wikis/home

The doxygen reference manual can be found here: https://roy.pages.obspm.fr/pFoF/index.html

If you use pFoF for scientific work, we kindly ask you to reference the code paper on pFoF, i.e.  
pFoF : a highly scalable halo-finder for large cosmological data sets, F. Roy, V. Bouillot, Y. Rasera, A&A 564, A13 (2014)

## Install

pFoF needs:
* MPI: it has been tested with OpenMPI and IntelMPI  
* HDF5: parallel version with support of Fortran 2003  

It comes with some GNU Make makefiles. You should edit the Make.inc file to edit the options according to your configuration.

More info on the wiki pages https://gitlab.obspm.fr/roy/pFoF/wikis/pfof_snap and https://gitlab.obspm.fr/roy/pFoF/wikis/pfof_cone.

## Test

Get the code sources from this Git repository (you can use the download button under the project name on the project home page or use the command 
`git clone https://gitlab.obspm.fr/roy/pFoF.git`).  
Go to pFoF/trunk/pfof_snap/src.  
Build the code.  
Get the test data (see the wiki page).  
Copy the executable (default name: pfof_snap) to the test directory.  
Run pfof_snap with  
mpirun -np 8 ./pfof_snap

More info on the wiki page https://gitlab.obspm.fr/roy/pFoF/wikis/test-page.  

## License

pFoF is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  
pFoF is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

