! Copyright  2017 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

Program conegravcreator

  Use modparam
  Use modio
  Use modsortpart
  Use modvariable
  Use mpi
  Use modhdf5

  Implicit None

  Integer :: ilvl, fc, lc

  Call Mpi_Init(mpierr)
  Call Mpi_Comm_Rank(Mpi_Comm_World, procID, mpierr)
  Call Mpi_Comm_Size(Mpi_Comm_World, procNB, mpierr)

  Call hdf5_init()

  Call readparameters

  Call readramsesfiles

  Call dividespace()

  fc = 1
  lc = 0
  ! we sort each level independantly
  Do ilvl = 1, param%nlevel
     lc = lc + ncellperlevel(ilvl)
     If(ncellperlevel(ilvl) > 0) Then
        Call tritas(ncellperlevel(ilvl),idcube(fc:lc),pos(:,fc:lc),acc(:,fc:lc),&
             phi(fc:lc),rho(fc:lc),refined(fc:lc))
        fc = fc + ncellperlevel(ilvl)
     End If
  End Do

#ifdef WITHMPI3
  Call Mpi_Wait(req_sumnpc, mpistat, mpierr)
#endif


  Call h5writeconegrav()
     
  Deallocate(pos, acc, rho, phi, refined)
  Deallocate(ncelltab)

  Call hdf5_finalize()

  If(procID==0) Then
     Call theend()
  End If

  Call Mpi_Finalize(mpierr)

End Program conegravcreator
