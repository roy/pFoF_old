! Copyright  2017 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

Program testextract

  Use modextract
  Use modvariables

  Implicit none

  type = 'cuboid'
  filename = '/data/home/roy/Cosmo/Test/pfof_cube_snap_part_data_testsorted'

  center(1) = 0.23
  center(2) = 0.64
  center(3) = 0.51

  dimensions(1) = 0.15
  dimensions(2) = 0.15
  dimensions(3) = 0.15

  Call extract_cuboid()

  Print *,'Centre:',center
  Print *,'Pos? ',size(pos,1), size(pos,2), maxval(pos(1,:)), maxval(pos(2,:)), maxval(pos(3,:)),  minval(pos(1,:)), minval(pos(2,:)), minval(pos(3,:))


End Program testextract
