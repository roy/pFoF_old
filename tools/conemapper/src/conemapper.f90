! Copyright  2017 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

Program conemapper

  Use modhdf5
  Use modio
  Use modmap

  Implicit None

  ! Initialization of HDF5
  Call hdf5_init()

  Call readparameters()

  Call h5readshellinfo()

  Call exploremap()

  ! Finalize HDF5
  Call hdf5_finalize()

  Call theend()

End Program conemapper
