! Copyright  2007-2017 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Input parameters for pfof_snap.
!! @brief
!! The parameters are stored in a user-defined data type Type_parameter_pfof_snap.<br>
!! This datatype is defined in common/src/modconstant.f90
!> @author Fabrice Roy 

!> Input parameters for pfof_snap. <br>
Module modparameters

  Use modconstant
  Implicit None

  Type(Type_parameter_pfof_snap) :: param !< Input parameters for pfof_snap.

End Module modparameters
