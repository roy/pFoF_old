! Copyright  2007-2014 Fabrice Roy, Vincent Bouillot, Yann Rasera
! Copyright  2017 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of pFoF.
!
! pFoF is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! pFoF is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with pFoF. If not, see <http://www.gnu.org/licenses/>.

!> pFOF_cone is a distributed implementation of the Friends-of-Friends halo finder algorithm designed for lightcones simulated with RAMSES. <br>
!! This parallel Friend of Friend implementation is based on a sequential
!! implementation written by Edouard Audit (Maison de la Simulation). <br>
!! It has been written by Fabrice Roy, Vincent Bouillot and Yann Rasera (LUTH / CNRS / Observatoire de Paris / PSL Research University / Université Paris Diderot). <br>
!! Contact: fabrice.roy@obspm.fr <br>

Program pfof_cone

  Use modhdf5
  Use modio
  Use modvariables
  Use modparameters
  Use modmpicom
  Use modfofpara

  Implicit None

  ! Initialization of MPI
  Call Mpi_Init(mpierr)
  Call Mpi_Comm_Size(Mpi_Comm_World, procNB, mpierr)
  Call Mpi_Comm_Rank(Mpi_Comm_World, procID, mpierr)

  If(procID==0) Then
     Call title()
  End If

  ! Initialization of HDF5
  Call hdf5_init()

  Call readparameters()

  Call h5readmap()

  Call h5readshellinfo()

  Call h5readparticles()

  Call computeminmax()

  Call fofparacone()

  Call deallocall()

  ! Finalize HDF5
  Call hdf5_finalize()

  If(procID==0) Then
     Call theend()
  End If

  ! Finalize MPI
  Call Mpi_Finalize(mpierr)

End Program pfof_cone
